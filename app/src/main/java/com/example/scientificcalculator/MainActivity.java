package com.example.scientificcalculator;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    Button b2nd, brad, bsin, bcos, btan, bxpowery, blog, bln, bpar1, bpar2, bxsqrt,
            bac, bparcentage, bdiv, bfact,
            bdot, bpi, bequals, bplus, bmin, bmul, binv,
            be,
            b1, b2, b3, b4, b5, b6, b7, b8, b9, b0;
    LinearLayout ll1;
    TextView tvfirst, tvsec;
    String pi = "3.14";
    String e = "2.71";
    ImageButton backspace, maxmin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        tvfirst = findViewById(R.id.tvfrst);
        tvsec = findViewById(R.id.tvsec);


        b2nd = findViewById(R.id.bt2nd);
        b2nd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tvsec.setText(tvsec.getText() + "2nd");
            }
        });

        brad = findViewById(R.id.btrad);
        brad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tvsec.setText(tvsec.getText() + "rad");
            }
        });

        bsin = findViewById(R.id.btsin);
        bsin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tvsec.setText(tvsec.getText() + "sin");
            }
        });

        bcos = findViewById(R.id.btcos);
        bcos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tvsec.setText(tvsec.getText() + "cos");
            }
        });

        btan = findViewById(R.id.bttan);
        btan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tvsec.setText(tvsec.getText() + "tan");
            }
        });

        bxpowery = findViewById(R.id.btxy);
        bxpowery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                double d = Double.parseDouble(tvsec.getText().toString());
                double square = d * d;
                tvsec.setText(String.valueOf("=" + square));
                tvfirst.setText(d + "²");
            }
        });

        blog = findViewById(R.id.btlg);
        blog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tvsec.setText(tvsec.getText() + "log");
            }
        });

        bln = findViewById(R.id.btln);
        bln.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tvsec.setText(tvsec.getText() + "ln");
            }
        });

        bpar1 = findViewById(R.id.btpar1);
        bpar1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tvsec.setText(tvsec.getText() + "(");
            }
        });

        bpar2 = findViewById(R.id.btpar2);
        bpar2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tvsec.setText(tvsec.getText() + ")");
            }
        });

        bxsqrt = findViewById(R.id.btrtx);
        bxsqrt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String str = tvsec.getText().toString();
                double d = Math.sqrt(Double.parseDouble(str));
                tvsec.setText(String.valueOf("=" + d));
            }
        });

        bac = findViewById(R.id.btac);
        bac.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tvsec.setText("");
                tvfirst.setText("");
            }
        });
        //bspace=findViewById(R.id.btback);
        bparcentage = findViewById(R.id.btper);
        bparcentage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tvsec.setText(tvsec.getText() + "%");
            }
        });

        bdiv = findViewById(R.id.btdiv);
        bdiv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tvsec.setText(tvsec.getText() + "/");
            }
        });

        bfact = findViewById(R.id.btxext);
        bfact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int val = Integer.parseInt(tvsec.getText().toString());
                int fact = factorial(val);
                tvsec.setText(String.valueOf("=" + fact));
                tvfirst.setText(val + "!");
            }
        });


        bdot = findViewById(R.id.btpoint);
        bdot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tvsec.setText(tvsec.getText() + ".");
            }
        });

        bpi = findViewById(R.id.btpi);
        bpi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tvfirst.setText(bpi.getText());
                tvsec.setText("=" + pi);

            }
        });

        bequals = findViewById(R.id.btequal);
        bequals.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String val = tvsec.getText().toString();
                String replacedstr = val.replace('÷', '/').replace('x', '*');
                double result = equal(replacedstr);
                tvsec.setText(String.valueOf("=" + result));
                tvfirst.setText(val);
            }
        });

        bplus = findViewById(R.id.btplus);
        bplus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tvsec.setText(tvsec.getText() + "+");
            }
        });

        bmin = findViewById(R.id.btminus);
        bmin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tvsec.setText(tvsec.getText() + "-");
            }
        });

        bmul = findViewById(R.id.btmul);
        bmul.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tvsec.setText(tvsec.getText() + "x");
            }
        });

        binv = findViewById(R.id.btonebyx);
        binv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tvsec.setText(tvsec.getText() + "^");
            }
        });


        b1 = findViewById(R.id.btone);
        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tvsec.setText(tvsec.getText() + "1");
            }
        });

        b2 = findViewById(R.id.bttwo);
        b2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tvsec.setText(tvsec.getText() + "2");
            }
        });

        b3 = findViewById(R.id.btthree);
        b3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tvsec.setText(tvsec.getText() + "3");
            }
        });

        b4 = findViewById(R.id.btfour);
        b4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tvsec.setText(tvsec.getText() + "4");
            }
        });

        b5 = findViewById(R.id.btfive);
        b5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tvsec.setText(tvsec.getText() + "5");
            }
        });

        b6 = findViewById(R.id.btsix);
        b6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tvsec.setText(tvsec.getText() + "6");
            }
        });

        b7 = findViewById(R.id.btseven);
        b7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tvsec.setText(tvsec.getText() + "7");
            }
        });

        b8 = findViewById(R.id.bteight);
        b8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tvsec.setText(tvsec.getText() + "8");
            }
        });

        b9 = findViewById(R.id.btnine);
        b9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tvsec.setText(tvsec.getText() + "9");
            }
        });

        b0 = findViewById(R.id.btzero);
        b0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tvsec.setText(tvsec.getText() + "0");
            }
        });

        be = findViewById(R.id.bte);
        be.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tvsec.setText("=" + e);
                tvfirst.setText(be.getText());

            }
        });

        backspace = findViewById(R.id.btback);
        backspace.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                String val = tvsec.getText().toString();
                val = val.substring(0, val.length() - 1);
                tvsec.setText(val);

            }
        });

        maxmin = findViewById(R.id.btmaxandmin);
        ll1 = findViewById(R.id.ll1);
        maxmin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Button b1 = findViewById(R.id.bt2nd);
//                b1.setVisibility(View.VISIBLE);
                if (b1.getVisibility() == View.GONE) {
                    b1.setVisibility(View.VISIBLE);
                } else {
                    b1.setVisibility(View.GONE);
                }

                Button b2 = findViewById(R.id.btrad);
//                b2.setVisibility(View.VISIBLE);
                if (b2.getVisibility() == View.GONE) {
                    b2.setVisibility(View.VISIBLE);
                } else {
                    b2.setVisibility(View.GONE);
                }

                Button b3 = findViewById(R.id.btsin);
//                b3.setVisibility(View.VISIBLE);
                if (b3.getVisibility() == View.GONE) {
                    b3.setVisibility(View.VISIBLE);
                } else {
                    b3.setVisibility(View.GONE);
                }


                Button b4 = findViewById(R.id.btcos);
//                b4.setVisibility(View.VISIBLE);
                if (b4.getVisibility() == View.GONE) {
                    b4.setVisibility(View.VISIBLE);
                } else {
                    b4.setVisibility(View.GONE);
                }

                Button b5 = findViewById(R.id.bttan);
//                b5.setVisibility(View.VISIBLE);
                if (b5.getVisibility() == View.GONE) {
                    b5.setVisibility(View.VISIBLE);
                } else {
                    b5.setVisibility(View.GONE);
                }

                Button b6 = findViewById(R.id.btxy);
//                b6.setVisibility(View.VISIBLE);
                if (b6.getVisibility() == View.GONE) {
                    b6.setVisibility(View.VISIBLE);
                } else {
                    b6.setVisibility(View.GONE);
                }

                Button b7 = findViewById(R.id.btlg);
//                b7.setVisibility(View.VISIBLE);
                if (b7.getVisibility() == View.GONE) {
                    b7.setVisibility(View.VISIBLE);
                } else {
                    b7.setVisibility(View.GONE);
                }

                Button b8 = findViewById(R.id.btln);
//                b8.setVisibility(View.VISIBLE);
                if (b8.getVisibility() == View.GONE) {
                    b8.setVisibility(View.VISIBLE);
                } else {
                    b8.setVisibility(View.GONE);
                }

                Button b9 = findViewById(R.id.btpar1);
//                b9.setVisibility(View.VISIBLE);
                if (b9.getVisibility() == View.GONE) {
                    b9.setVisibility(View.VISIBLE);
                } else {
                    b9.setVisibility(View.GONE);
                }

                Button b10 = findViewById(R.id.btpar2);
//                b10.setVisibility(View.VISIBLE);
                if (b10.getVisibility() == View.GONE) {
                    b10.setVisibility(View.VISIBLE);
                } else {
                    b10.setVisibility(View.GONE);
                }

                Button b11 = findViewById(R.id.btrtx);
//                b11.setVisibility(View.VISIBLE);
                if (b11.getVisibility() == View.GONE) {
                    b11.setVisibility(View.VISIBLE);
                } else {
                    b11.setVisibility(View.GONE);
                }

                Button b12 = findViewById(R.id.btxext);
//                b12.setVisibility(View.VISIBLE);
                if (b12.getVisibility() == View.GONE) {
                    b12.setVisibility(View.VISIBLE);

                } else {
                    b12.setVisibility(View.GONE);
                }

                Button b13 = findViewById(R.id.btonebyx);
//                b13.setVisibility(View.VISIBLE);
                if (b13.getVisibility() == View.GONE) {
                    b13.setVisibility(View.VISIBLE);
                } else {
                    b13.setVisibility(View.GONE);
                }

                Button b14 = findViewById(R.id.btpi);
//               b14.setVisibility(View.VISIBLE);
                if (b14.getVisibility() == View.GONE) {
                    b14.setVisibility(View.VISIBLE);
                } else {
                    b14.setVisibility(View.GONE);
                }

                Button b15 = findViewById(R.id.bte);
                // b15.setVisibility(View.VISIBLE);


                if (b15.getVisibility() == View.GONE) {
                    b15.setVisibility(View.VISIBLE);
                } else {
                    b15.setVisibility(View.GONE);

                }


            }


//                b2nd.setVisibility(View.GONE);
//                brad.setVisibility(View.GONE);
//                bsin.setVisibility(View.GONE);
//                bcos.setVisibility(View.GONE);
//                btan.setVisibility(View.GONE);
//
//                bxpowery.setVisibility(View.GONE);
//                blog.setVisibility(View.GONE);
//                bln.setVisibility(View.GONE);
//                bpar1.setVisibility(View.GONE);
//                bpar2.setVisibility(View.GONE);
//
//                bxsqrt.setVisibility(View.GONE);
//                bfact.setVisibility(View.GONE);
//                binv.setVisibility(View.GONE);
//                bpi.setVisibility(View.GONE);

        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.calz_menu, menu);
        return super.onCreateOptionsMenu(menu);



    }
    public void history(){
        Button button=findViewById(R.id.history);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
    }



    //factorial function
    int factorial(int n) {
        return (n == 1 || n == 0) ? 1 : n * factorial(n - 1);
    }


    public static double equal(final String str) {
        return new Object() {
            int pos = -1, ch;

            void nextChar() {
                ch = (++pos < str.length()) ? str.charAt(pos) : -1;
            }

            boolean bt(int charT) {
                while (ch == ' ') nextChar();
                if (ch == charT) {
                    nextChar();
                    return true;
                }
                return false;
            }

            double parse() {
                nextChar();
                double x = parseExpression();
                if (pos < str.length()) throw new RuntimeException("Unexpected: " + (char) ch);
                return x;
            }


            double parseExpression() {
                double x = parseTerm();
                for (; ; ) {
                    if (bt('+')) x += parseTerm(); // addition
                    else if (bt('-')) x -= parseTerm(); // subtraction
                    else return x;
                }
            }

            double parseTerm() {
                double x = parseFactor();
                for (; ; ) {
                    if (bt('*')) x *= parseFactor(); // multiplication
                    else if (bt('/')) x /= parseFactor(); // division
                    else return x;
                }
            }

            double parseFactor() {
                if (bt('+')) return parseFactor(); // unary plus
                if (bt('-')) return -parseFactor(); // unary minus

                double x;
                int startPos = this.pos;
                if (bt('(')) { // parentheses
                    x = parseExpression();
                    bt(')');
                } else if ((ch >= '0' && ch <= '9') || ch == '.') { // numbers
                    while ((ch >= '0' && ch <= '9') || ch == '.') nextChar();
                    x = Double.parseDouble(str.substring(startPos, this.pos));
                } else if (ch >= 'a' && ch <= 'z') { // functions
                    while (ch >= 'a' && ch <= 'z') nextChar();
                    String func = str.substring(startPos, this.pos);
                    x = parseFactor();
                    if (func.equals("sqrt")) x = Math.sqrt(x);
                    else if (func.equals("sin")) x = Math.sin(Math.toRadians(x));
                    else if (func.equals("cos")) x = Math.cos(Math.toRadians(x));
                    else if (func.equals("tan")) x = Math.tan(Math.toRadians(x));
                    else if (func.equals("log")) x = Math.log10(x);
                    else if (func.equals("ln")) x = Math.log(x);
                    else throw new RuntimeException("Unknown function: " + func);
                } else {
                    throw new RuntimeException("Unexpected: " + (char) ch);
                }

                if (bt('^')) x = Math.pow(x, parseFactor()); // exponentiation

                return x;
            }
        }.parse();
    }

}